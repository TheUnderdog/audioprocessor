#Version 1.7.1 [CCAPI Edition]

import os
import bz2
import glob
from pathlib import Path
from functools import partial

def RemoveEndSlashFromDirectory(IncomingDirectoryString):
	
	ScanPoint = len(IncomingDirectoryString)-1
	
	while ScanPoint > -1:
		
		if IncomingDirectoryString[ScanPoint] != "\\" and IncomingDirectoryString[ScanPoint] != "/":
			break
		
		ScanPoint -= 1
		
	return IncomingDirectoryString[:ScanPoint+1]

def RecursivelyMakeDirectories(IncomingPathname):
	TempPath = Path(IncomingPathname)
	TempPath.mkdir(parents=True,exist_ok=True)

def GetParentDirectory(IncomingPathname):
	TempPath = Path(IncomingPathname)
	return TempPath.parent.absolute()

#In bytes
def FileSize(IncomingFilename):
	return os.path.getsize(IncomingFilename)

def BytesIOSize(IncomingBytesIO):
	return IncomingBytesIO.getbuffer().nbytes

def FileAccessAndModificationTime(IncomingFilename):
	TempStat = os.stat(IncomingFilename)
	return (TempStat.st_atime,TempStat.st_mtime)

def FileModificationTime(IncomingFilename):
	return os.path.getmtime(IncomingFilename)

def FileExists(IncomingFilename):
	return os.path.isfile(IncomingFilename)
	
def DeleteFile(Filename):
	if os.path.isfile(Filename):
		os.remove(Filename)
		
def RenameFile(OldFilename,NewFilename):
	if os.path.isfile(OldFilename):
		os.rename(OldFilename,NewFilename)
		
def DeleteEmptyDirectory(Dirname):
	os.rmdir(Dirname)

def GlobAllFiles(GlobString):
	return [f for f in glob.glob(GlobString)]
	
def NormaliseSlashes(FullFilename,OptionalForwardSlashToBack):
	
	if OptionalForwardSlashToBack is not None:
		return FullFilename.replace("\\","/");
	
	else:
		return FullFilename.replace("/","\\")
		
def DetectSlashes(FullFilename):
	DetectType = 0
	
	if '\\' in FullFilename:
		DetectType += 1
		
	if '/' in FullFilename:
		DetectType += 2

	return DetectType
	
def ExtractFilenameDirectory(FullFilename):	
	EndFilename = ExtractEndFilename(FullFilename)
	return FullFilename[:len(FullFilename) - len(EndFilename)]
	
def ExtractEndFilename(FullFilename):
	Normalised = NormaliseSlashes(FullFilename,True);
	DetectType = DetectSlashes(Normalised)
	
	if DetectType == 0:
		return Normalised
	elif DetectType == 1:
		return Normalised[Normalised.rindex('\\')+1:]
	elif DetectType == 2:
		return Normalised[Normalised.rindex('/')+1:]
	else:
		raise ValueError('FullFilename slashes were not normalised',FullFilename)
		
def ExtractEXT(FullFilename):
	return FullFilename[FullFilename.rindex('.')+1:]

def RemoveEXT(FullFilename):
	return FullFilename[:FullFilename.rindex('.')]
	
def GetDefaultSlash(FullFilename):
	DetectType = etectSlashes(FullFilename)
	
	if DetectType == 1:
		return '\\'
	else:
		return '/'
		
def WriteCompressedTextFile(Filename,IncomingTextData,*,TextEncoding='utf-8',WriteMode='wb',CompressionLevel=9):
	
	OutgoingBytes = TextToBytes(IncomingTextData,TextEncoding)
	WriteCompressedFile(Filename,OutgoingBytes,WriteMode,CompressionLevel)
	
def WriteCompressedFile(Filename,IncomingBinaryData,WriteMode='wb',CompressionLevel=9):
	
	with bz2.open(Filename,WriteMode,CompressionLevel) as OpenFile:
		OpenFile.write(IncomingBinaryData)
		
def ReadCompressedTextFile(Filename,OptionalTextEncoding='utf-8'):
	
	IncomingBytes = ReadCompressedFile(Filename)
	return BytesToText(IncomingBytes,OptionalTextEncoding)
	
def ReadCompressedFile(Filename,ReadMode='rb'):
	
	with bz2.open(Filename,ReadMode) as OpenFile:
		return OpenFile.read()
		
def CompressFromSourceFilenameBuffer(Filename):
	
	TempCompress = bz2.BZ2Compressor(9)
	BinaryData = b""
	
	with open(Filename,"rb") as TempFile:
		for ByteBuffer in iter(partial(TempFile.read, 4096), b""):
			BinaryData += TempCompress.compress(ByteBuffer)
			
		BinaryData += TempCompress.flush()
		return BinaryData

def CompressFromSourceFilename(Filename):
	
	with open(Filename, 'rb') as TempFile:
		return bz2.compress(TempFile.read(),9)

def DecompressToSourceFilename(Filename,CompressedData):
	
	with open(Filename, 'wb') as TempFile:
		TempFile.write(bz2.decompress(CompressedData))

def WriteFileLines(Filename,Lines,*,WriteMode='w',AppendNewLine='\n'):
	
	with open(Filename,WriteMode) as OpenFile:
		if AppendNewLine is not None:
			for Line in Lines:
				OpenFile.write(Line+AppendNewLine)
		else:
			OpenFile.writelines(Lines)
			
def ReadFile(Filename):
	with open(Filename,'r') as OpenFile:
		return OpenFile.read()
		
def ReadFileLines(Filename,*,StripWhiteSpace=True):
	
	Collector = []
	with open(Filename) as OpenFile:
		FileLines = OpenFile.readlines()
		
		if StripWhiteSpace:
			for EachLine in FileLines:
				Collector.append(EachLine.strip())
		else:
			Collector = FileLines
		
	return Collector
		
def WriteDictionary(Filename,IncomingDictionary,*,SplitChar=':',WriteMode='w',AppendNewLine='\n'):
	
	Collector = []
	
	for Key in IncomingDictionary:
		Collector.append(Key+SplitChar+str(IncomingDictionary[Key]))
	
	WriteFileLines(Filename,Collector,WriteMode,AppendNewLine=AppendNewLine)
	
def ReadDictionary(Filename,*,SplitChars=':',OptionalStripChars=False):
	NewLines = ReadFileLines(Filename)

	NewDictionary = {}
	
	for EachLine in NewLines:
		Temp = EachLine.split(SplitChars)
		try:
			if Temp is not None:
				if len(Temp) == 2:
					if OptionalStripChars:
						NewDictionary[Temp[0]] = Temp[1].strip()
					else:
						NewDictionary[Temp[0]] = Temp[1]
				else:
					print("WARNING: "+filename+" gave a Temp length not matching 2 (some sort of file formatting issue?)")
			else:
				print("WARNING: "+filename+" gave a Temp was none (some sort of file formatting issue?)")
			
		except:
			raise ValueError('Split error in ' + EachLine + '. Possibly missing a split char? In this case: ' + SplitChars)
			
	return NewDictionary
