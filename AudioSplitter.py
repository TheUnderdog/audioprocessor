import re
import glob
import argparse
import subprocess
from pathlib import Path
from FileIOA import *

G_ValidVideoExt = ["mp4","m4a","flv","webm","mov","MOV","ogg","ogv"]

def SplitAudioStream(SourceFilename,TargetFilename,ListOfStreamNumbers=[0]):
	
	EXT = ExtractEXT(TargetFilename)
	FilenameWithoutEXT = RemoveEXT(TargetFilename)
	
	CommandString = "ffmpeg -i '"+SourceFilename+"'"
	
	for EachStreamNumber in ListOfStreamNumbers:
		StreamNumber = str(EachStreamNumber)
		CommandString += " -map 0:"+StreamNumber+" -acodec copy '"+FilenameWithoutEXT+"_Stream"+StreamNumber+"."+EXT+"'"
	
	print(CommandString+"\n\n")
	subprocess.call(CommandString,shell=True)

def BulkSplitAudioInDirectory(InputDirectory,OutputDirectory,OutputEXT="mp3",ListOfStreamNumbers=[0],OptionalDeleteAfterConversion=False):
	
	ValidVideoExt = G_ValidVideoExt
	
	for VideoExt in ValidVideoExt:
		
		GlobString = os.path.join(InputDirectory,"*."+VideoExt)
		print(GlobString)
		ListOfFiles = GlobAllFiles(GlobString);
	
		for FileInList in ListOfFiles:
			TempFilenameWithoutExt = RemoveEXT(ExtractEndFilename(FileInList))
			AudioFilename = os.path.join(OutputDirectory,TempFilenameWithoutExt+"."+OutputEXT)
			
			SplitAudioStream(FileInList,AudioFilename,ListOfStreamNumbers)
			
			if OptionalDeleteAfterConversion:
				DeleteFile(FileInList)
				
def CheckIfRelative(IncomingString):
	return bool(re.match(r'^\.[\\/]', IncomingString))

if __name__ == "__main__":
	CLIParser = argparse.ArgumentParser(description="Scan a directory for audio files and then split the audio streams.")	
	CLIParser.add_argument("-i", "--inputdir", help="The directory to look for audio files in",required=True)
	CLIParser.add_argument("-o", "--outputdir", help="The directory to place the output split streams",required=True)
	CLIParser.add_argument("-e", "--ext", help="The audio extension output to use (default=mp3)",default="mp3")
	CLIParser.add_argument("-l", "--listofstreamnumbers", help="A comma separated list of Stream numbers to extract (example:1,2,4; default=0)",default="0")
	CLIParser.add_argument("-d", "--delete", help="Whether to delete the source files post conversion (default=False)",default="False")
	
	CLIArgs = CLIParser.parse_args()
	
	InputDir = str(CLIArgs.inputdir)
	OutputDir = str(CLIArgs.outputdir)
	
	if CheckIfRelative(InputDir):
		
		InputDir = InputDir[2:]
		InputDir = os.path.join(os.path.realpath(os.getcwd()), InputDir)

	if CheckIfRelative(OutputDir):
				
		OutputDir = OutputDir[2:]
		OutputDir = os.path.join(os.path.realpath(os.getcwd()), OutputDir)
	
	
	TempList = str(CLIArgs.listofstreamnumbers).split(",")
	NewList = []
	for EachItem in TempList:
		NewList.append(int(EachItem))
	
	DeleteBool = False
	DeleteLower = CLIArgs.delete.lower()
	
	if DeleteLower == "true" or DeleteLower == "yes" or DeleteLower == "y" or DeleteLower == "t":
		DeleteBool = True
	
	BulkSplitAudioInDirectory(
								InputDir,
								OutputDir,
								CLIArgs.ext,
								NewList,
								DeleteBool
								)
