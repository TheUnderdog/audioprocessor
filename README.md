#Comment to stop GitLab deleting this stuff


# AudioProcessor

For processing audio for videos

# Usage

**Audio Splitter**:

`python3 AudioSplitter.py -i --inputdir -o --outputdir -e --ext -l --listofstreamnumbers -d --delete`

Example: `python3 AudioSplitter.py -i "./Incoming Audio" -o './Split Audio' -e ogg -l 1,2 -d False`

**Audio Appender**:

`python3 AudioAppender.py -i --inputdir -t --targetfile -ao --appendorder -d --delete`

Example: `python3 AudioAppender.py -i "./Audio to Append" -t "./Audio Appended/Test.ogg" -ao 2,1 -d False`

Note: Usage of "./" or ".\\" at the **start** of a directory or targetfile string is permitted, and is interpreted as meaning 'current directory' where you are currently executing the script from.
