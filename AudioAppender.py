import os
import re
import glob
import argparse
import subprocess
from FileIOA import *

G_ValidAudioExt = ["ogg","mp3","wav"]

def AudioAppend(ListOfFilenames,OutgoingFilename,OptionalDeleteAfterAppend=False):
	
	CmdStr = "sox "
	
	for Item in ListOfFilenames:
		CmdStr = CmdStr + "'" + str(Item) + "' "
		
	subprocess.call(CmdStr+" '"+str(OutgoingFilename)+"'",shell=True)
	
	if OptionalDeleteAfterAppend:
		for Item in ListOfFilenames:
			DeleteFile(Item)

def QueryAudioList(TargetDirectory):
	ValidAudioExt = G_ValidAudioExt
	
	FilePairing = []
	
	for AudioExt in ValidAudioExt:
		
		GlobString = os.path.join(TargetDirectory,"*."+AudioExt)
		
		ListOfFiles = GlobAllFiles(GlobString);
		Iter = 1
		
		for FileInList in ListOfFiles:
			FilePairing.append([str(Iter), ExtractEndFilename(FileInList), FileInList])
			Iter += 1
			
	return FilePairing

def GetUserListInput(Prompt,MaxValue):
	
	while True:
		IntValues = []
		UserInput = input(Prompt)
		TextValues = UserInput.split(",")
		try:
			
			for TempInt in TextValues:
				InputValue = int(TempInt.strip())
				
				if InputValue < 1:
					raise ValueError('Number cannot be lower than 1')
				
				if InputValue > MaxValue:
					raise ValueError('Number cannot be higher than '+str(MaxValue))
				
				IntValues.append(InputValue)
			break
		except ValueError as Err:
			print(Err)
			print("Please enter a list of integers.")
		except:
			print("Unknown error. Try again.")
			
	return IntValues

def GetUserInput(Prompt):
	UserInput = None
	
	while True:
		UserInput = input(Prompt)
		try:
			
			if CheckIfRelative(UserInput):
				UserInput = UserInput[2:]
				UserInput = os.path.join(os.path.realpath(os.getcwd()), UserInput)
			
			if FileExists(UserInput):
				raise ValueError('File already exists with that name.')
			
			break
		except ValueError as Err:
			print(Err)
			print("Please enter a target filename.")
		except:
			print("Unknown error. Try again.")
			
	return str(UserInput)

def CheckIfRelative(IncomingString):
	
	if IncomingString is None:
		return False
	
	return bool(re.match(r'^\.[\\/]', IncomingString))

def RunAppendUI(*,TargetDirectory,TargetFilename=None,AppendOrder=None,OptionalDeleteAfterAppend=False):
	
	print("Searching "+str(TargetDirectory)+" ...")
	FilePairing = QueryAudioList(TargetDirectory)
	
	print(FilePairing)
	
	if len(FilePairing) < 1:
		print("No audio files found.")
		return
	
	for EachItem in FilePairing:
		print(str(EachItem[0])+") "+str(EachItem[1]))
	
	if AppendOrder is None:
		AppendOrder = GetUserListInput("Input a set of comma-delimited number (EG 1,4,3) representing the append order:\n",len(FilePairing))
	
	FilesToAppend = []
	
	for EachOrder in AppendOrder:
		FilesToAppend.append(FilePairing[(EachOrder-1)][2])
	
	print(FilesToAppend)
	
	if TargetFilename is None:
		TargetFilename = GetUserInput("Input a target filename including directory to output to:\n")
	
	print("Appending...")
	AudioAppend(FilesToAppend,TargetFilename,OptionalDeleteAfterAppend)
	
	
if __name__ == "__main__":
	CLIParser = argparse.ArgumentParser(description="Scan a directory for audio files and then append the audio streams.")	
	CLIParser.add_argument("-i", "--inputdir", help="The directory to look for audio files in (default=.)",default=os.path.realpath(os.getcwd()))
	CLIParser.add_argument("-ao", "--appendorder", help="A comma-delimited list of numbers representing file order (default=None)",default=None)
	CLIParser.add_argument("-t", "--targetfilename", help="The target filename to save the file as. If missing will be prompted (default=None)",default=None)
	CLIParser.add_argument("-d", "--delete", help="Whether to delete the source files post append (default=False)",default="False")
	
	CLIArgs = CLIParser.parse_args()
	
	InputDir = str(CLIArgs.inputdir)
	TargetFilename = CLIArgs.targetfilename
	
	if CheckIfRelative(InputDir):
		
		InputDir = InputDir[2:]
		InputDir = os.path.join(os.path.realpath(os.getcwd()), InputDir)
		
	if CheckIfRelative(TargetFilename):
		
		TargetFilename = TargetFilename[2:]
		TargetFilename = os.path.join(os.path.realpath(os.getcwd()), TargetFilename)
	
	DeleteBool = False
	DeleteLower = str(CLIArgs.delete).lower()
	
	if DeleteLower == "true" or DeleteLower == "yes" or DeleteLower == "y" or DeleteLower == "t":
		DeleteBool = True
	
	RunAppendUI(
					TargetDirectory=InputDir,
					TargetFilename=TargetFilename,
					AppendOrder=CLIArgs.appendorder,
					OptionalDeleteAfterAppend=DeleteBool
				)

